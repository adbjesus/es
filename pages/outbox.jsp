<jsp:include page="header.jsp"/>
<div id="main" class="centered lowered">
	<%@page import="java.util.*"%>
	<%@page import="bd.*"%>
	<jsp:include page="sidebar_mensagens.jsp"/>
	<div id="content">
		<ul id="zebra_table">
			<ul id="row" class="title">
				<li id="elem1"> Recipient </li>
				<li id="elem2"> Title </li>
				<li id="elem3"> Date </li>
			</ul>
			<% 
			ArrayList<MensagemPrivada> outbox = (ArrayList) session.getAttribute("out"); 
			MensagemPrivada temp;
			String ev_od = "even";
			for (int i=0;i<outbox.size();i++){
				if(i%2 == 0){
					ev_od = "even";
				}
				else{
					ev_od = "odd";
				}
			temp= outbox.get(i);
			out.println("<ul id='row' class='" + ev_od+ "''><a href='/projecto-es/ViewMessage?var=outbox&id="
			+ Integer.toString(i) + "''><li id='elem1'>" 
			+ temp.getDestinatario().getUsername() + "</li><li id='elem2'>" 
			+ temp.getAssunto() + "</li><li id='elem3'>" 
			+ temp.getDate().toString() 
			+ "</li></a></ul>" );
		}
		%>
	</ul>
</div>
</div>
<jsp:include page="footer.jsp"/>