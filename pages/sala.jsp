<jsp:include page="header.jsp"/>
	<div id="main" class="centered lowered">
		<%@page import="java.util.ArrayList"%>
		<%@page import="bd.*"%>
		<%@page import="java.util.*"%>
		<h1 id = "mensagem_noti">Sala <%out.println(session.getAttribute("current_room"));%></h1>
		<form action="/projecto-es/pages/novo_post.jsp" method="post">
			<input class="button" type="submit" value="Criar Post"/>
		</form>
		<h1 id="mensagem_noti">Listagem de posts</h1>
		<ul id="navlist">
			<%
				ArrayList<Post> posts = (ArrayList) session.getAttribute("posts");
				ArrayList<Post> replies;
				if(posts.size()==0){
					out.println("<h1>Nao existem posts nesta sala</h1>");
				}else{
					for(int i=posts.size()-1;i>=0;i--){
						Date d = new Date();
						if(posts.get(i).getDate().compareTo(d) > 0 && !posts.get(i).getRemetente().getUsername().equals((String)session.getAttribute("theUser"))) continue;
						out.println("<div id='post'>");
						if(posts.get(i).getRemetente().getUsername().equals((String)session.getAttribute("theUser"))){
							if(posts.get(i).getDate().compareTo(d) > 0){
								out.println("<form id='delete' action='/projecto-es/ApagarPost' method='post'>");
								out.println("<input type='hidden' name='post_id' value='"+i+"'>");
								out.println("<input class='button' type='submit' value='Delete'>");
								out.println("</form>");
							}
							out.println("<form id='editar' action='/projecto-es/pages/editar_post.jsp' method='get'>");
							out.println("<input type='hidden' name='post_id' value='"+i+"'>");
							out.println("<input class='button' type='submit' value='Editar'>");
							out.println("</form>");
							out.println("<div style='clear: both; height:1px; background-color: #DBDBEA'></div>");
							
						}
						out.println("<div id='post_top'>");
						out.println("<div id='post_sender'>");
						out.println(posts.get(i).getRemetente().getUsername());
						out.println("</div>");
						out.println("<div id='post_date'>");
						out.println(posts.get(i).getDate().toString());
						out.println("</div>");
						out.println("</div>");
						out.println("<div id='post_body'>");
						out.println("<div id='post_image'>");
						if(posts.get(i).getImagePath()!=null){
							out.println("<img height='100px' src='/projecto-es/data/images/"+posts.get(i).getImagePath()+"'>");
						}
						out.println("</div>");
						out.println("<div id='post_text'>");
						out.println(posts.get(i).getCorpo());
						out.println("</div>");
						out.println("</div>");
						replies = posts.get(i).getReplies();
						out.println("<div id='replies'>");
						for(int k=0;k<replies.size();k++){
							out.println("<br><li id='user'>");
							out.println(replies.get(k).getRemetente().getUsername());
							out.println("</li>");
							out.println("<li id='text'>");
							out.println(replies.get(k).getCorpo());
							out.println("</li>");
						}
						out.println("</div>");
						out.println("<form id='reply' action='/projecto-es/ResponderPost' method='post'>");
						out.println("<input type='text' name='reply_text' placeholder='Resposta'>");
						out.println("<input type='hidden' name='post_id' value='"+i+"'>");
						out.println("<input class='button' type='submit' value='Responder'>");
						out.println("</form>");
						out.println("</div>");
						if(i!=0){
							out.println("<div id='division'></div>");
						}
					}
				}
			%>
		</ul>
		
</div>
<jsp:include page="footer.jsp"/>