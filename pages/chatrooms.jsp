<jsp:include page="header.jsp"/>
	<div id="content">
		<%@page import="java.util.ArrayList"%>
		<%@page import="bd.*"%>
		<h1 id = "mensagem_noti">Salas de Chat existentes</h1>
		<ul id="navlist">
			<%
				ArrayList <Sala> salas =(ArrayList <Sala>) session.getAttribute("rooms");

				if(salas.size()==0){
					out.println("Nao existem salas");
				}else{
					for(int i=0;i<salas.size();i++){
						out.println("<li><a href='/projecto-es/ListarPosts?room_id="+Integer.toString(i)+"'>"+salas.get(i).getNome()+"</a></li>");
					}
				}
			%>
		</ul>
	</div>
<jsp:include page="footer.jsp"/>