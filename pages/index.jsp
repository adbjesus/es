<jsp:include page="header_front.jsp"/>

	<div id="main" class="centered lowered">
		<div id="login_form" class="form">
			<h1>Login</h1>
			<form action="/projecto-es/Login" method="post">
				<input name="username_field" type="text" placeholder="Username">
				<input name="pass_field" type="password" placeholder="Password"/>
				<input class="button" type="submit" value="Login"/>
			</form>
		</div>
		<!--<div id="separator"></div>-->
		<div id="register_form" class="form">
			<h1>Register</h1>
			<form action="/projecto-es/Register" method="post">
				<input required name="username_field" type="text" placeholder="Username"/>
				<input required name="mail_field" type="text" placeholder="Email"/>
				<input required name="pass_field" type="password" placeholder="Password"/>
				<input name="age_field" type="text" placeholder="Age"/>
				<div id="frontier">
				<div class="radio_btn_text">Male</div>
				<input class="radio_btn" type="radio" name="gender_field" value="Masculino" checked/>
				<div class="radio_btn_text">Female</div>
				<input class="radio_btn" type="radio" name="gender_field" value="Feminino"/>
				</div>
				<input class="button" type="submit" value="Register"/>
			</form>
		</div>
	</div>
<jsp:include page="footer.jsp"/>
