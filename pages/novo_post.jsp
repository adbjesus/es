<jsp:include page="header.jsp"/>

<div id ="main" class="lowered centered">
	<h1 id="mensagem_noti">Criar novo post</h1>
	<form action="/projecto-es/CriarPost" method="post" enctype="multipart/form-data">
		<textarea name="post_text" type="text" placeholder="Post"></textarea>
		<div style="width: 200px">
			<input name="post_image" type="file" style="border:none"/>
			<input name="post_date" type="date"/>
			<input name="post_hour" type="text" placeholder="Hora"/>
			<input name="post_minute" type="text" placeholder="Minutos"/>
		</div>
		<input class="button" type="submit" value="Criar"/>
	</form>
</div>
<jsp:include page="footer.jsp"/>