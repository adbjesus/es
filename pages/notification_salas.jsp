<!DOCTYPE html>
<%@page import="bd.Message;"%>
<html>
<head>
	<meta http-equiv="refresh" content="1; url=/projecto-es/pages/cria_sala.jsp"> 
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../css/style.css" type="text/css">
</head>
<body>
<div id="container">
	<div id="header">
		<div class="centered foot_head grey">
			<div id="logo" class="grey">DeiBook</div>
		</div>
	</div>
	
	<div id="main" class="lowered centered">
		<% Message msg = (Message) session.getAttribute("msg"); %>
		<h1 class="notify"><%=msg.getMessage()%><h1>
	</div>
<div>
<jsp:include page="footer.jsp"/>


