<%
String username = (String)session.getAttribute("theUser");
if(username==null){
	pageContext.forward("index.jsp");
}

%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../css/style.css" type="text/css">
	<!--<link rel="stylesheet" href="css/style.css" type="text/css">-->
</head>
<body>
<div id="container">
	<div id="header" class="grey">
		<div class="centered foot_head">
			<div id="logo" class="left">DeiBook</div>
			<div id="header_list" class="right">
				<a href="/projecto-es/ListarSalas">Salas de comunicação</a>
				<a href="/projecto-es/pages/mensagens.jsp">Mensagens</a>
				<a href="/projecto-es/pages/profile.jsp">Perfil</a>
				<a href="/projecto-es/Logout">Sair</a>
			</div>
		</div>
	</div>