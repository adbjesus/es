<jsp:include page="header.jsp"/>
<div id="main" class="centered lowered">
	<%@page import="bd.*"%>
	<%
	MensagemPrivada in = (MensagemPrivada) session.getAttribute("in");
	String var = (String)session.getAttribute("var");
	System.out.println(var);
	%>
	<jsp:include page="sidebar_mensagens.jsp"/>

	<div id="content">
		<h1 id="mensagem_noti">Mensagem</h1>
		<div id= "msg_read_from">
			<b>De:</b> <%=in.getRemetente().getUsername()%>
		</div>
		<div id = "msg_read_title">
			<b>Assunto:</b> <%=in.getAssunto()%>;
		</div>
		<div id = "msg_read_body">
			<b>Corpo:</b>
			<p id= "msg_txt"><%=in.getCorpo()%></p>
		</div>
		
		<form method="get" action=<%if (var.equals("inbox")) {out.println("/projecto-es/CheckInbox");} else {out.println("/projecto-es/CheckOutbox");}%>>
			<input class="button right send" type="submit" value="Back"/>
		</a>
	</div>
</div>
<jsp:include page="footer.jsp"/>
