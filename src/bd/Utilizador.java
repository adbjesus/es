package bd;
import java.util.*;
import java.io.Serializable;

public class Utilizador implements Serializable{
	/*Atributos da classe*/
	String username;
	String password;
	String genero;
	String email;
	int idade;
	ArrayList <MensagemPrivada> recebidas;
	ArrayList <MensagemPrivada> enviadas;
	
	
	/*Construtor*/
	public Utilizador(String username, String password, String genero, String email, int idade){
		this.username = username;
		this.password = password;
		this.genero = genero;
		this.idade = idade;
		this.email = email;
		recebidas= new ArrayList<MensagemPrivada>();
		enviadas= new ArrayList<MensagemPrivada>();
	}
	
	/*Metodos da classe*/
	public String getUsername(){
		return this.username;
	}
	public String getPass(){
		return this.password;
	}
	public String getEmail(){
		return this.email;
	}
	public int getIdade(){
		return this.idade;
	}
	public String getGenero(){
		return this.genero;
	}
	public void setNewSendMessage(MensagemPrivada temp){
		this.enviadas.add(temp);
	}
	public void setNewReceivedMessage(MensagemPrivada temp){
		this.recebidas.add(temp);
	}
	public ArrayList<MensagemPrivada> getInbox(){
		return this.recebidas;
	}
	public ArrayList<MensagemPrivada> getOutbox(){
		return this.enviadas;
	}
}
