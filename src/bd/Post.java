package bd;
import java.io.*;
import java.util.*;
import java.lang.*;
import java.nio.channels.FileChannel;
import java.io.Serializable;
import servlets.*;

public class Post extends Conteudo implements Serializable{
	private String imagePath = null;
	private ArrayList<Post> replies;

	public Post(Utilizador remetente,String corpo, Date dataDePublicacao, File image, String imagePath){
		super(remetente,corpo,dataDePublicacao);
		if(image!=null){
			this.imagePath = saveImage(image,imagePath);
		}
		replies = new ArrayList<>();
	}

	public void editPost(String corpo, File image, String imagePath){
		super.corpo = corpo;
		this.imagePath = saveImage(image,imagePath);
	}

	public String getImagePath(){
		return this.imagePath;
	}

	public String saveImage(File image,String imagePath){
		if(image==null || imagePath==null){
			return null;
		}
		File file = null;
		try{
			file = new File(ServletInitializer.imagePath+imagePath);
			File parent = file.getParentFile();
			if(!parent.exists() && !parent.mkdirs()){
    			throw new IllegalStateException("Couldn't create dir: " + parent);
			}
			if(!file.exists()){
  				file.createNewFile();
  			}
  			FileChannel source = null;
			FileChannel destination = null;
			source = new FileInputStream(image).getChannel();
			destination = new FileOutputStream(file).getChannel();
			destination.transferFrom(source, 0, source.size());
			source.close();
			destination.close();
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
		return imagePath;
	}

	public void addReply(Post tmp){
		replies.add(tmp);
	}

	public ArrayList<Post> getReplies(){
		return this.replies;
	}


}