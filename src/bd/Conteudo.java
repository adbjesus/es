package bd;
import java.util.*;
import java.io.Serializable;

public class Conteudo implements Serializable{
	/*Atributos da Classe*/
	Utilizador remetente;
	String corpo;
	Date dataDePublicacao;
	
	/*Construtor da Classe*/
	public Conteudo(Utilizador remetente,String corpo, Date dataDePublicacao){
		this.remetente = remetente;
		this.corpo = corpo;
		this.dataDePublicacao = dataDePublicacao;
		
	}
	
	/*Metodos da Classe*/
	public Utilizador getRemetente(){
		return this.remetente;
	}
	public String getCorpo(){
		return this.corpo;
	}
	public Date getDate(){
		return this.dataDePublicacao;
	}
}