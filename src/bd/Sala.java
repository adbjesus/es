package bd;
import java.io.Serializable;
import java.util.*;

public class Sala implements Serializable{
	/*Atributos da classe*/
	String nome;
	Date dataDeCriacao;
	boolean estado;
	ArrayList <Post> posts;
	ArrayList <Utilizador> menbros;
	ArrayList <Publicidade> publicidade;
	ArrayList <Classificacao> classificacoes;
	int maxPosts;
	int cliques;
	
	/*Construtor da classe*/
	public Sala(String nome, int maxPosts, Date dataDeCriacao){
		this.nome = nome;
		this.maxPosts = maxPosts;
		this.estado=true;
		this.dataDeCriacao = dataDeCriacao;
		this.posts = new ArrayList<Post>();
	}

	public String getNome(){
		return this.nome;
	}

	public ArrayList<Post> getPosts(){
		return this.posts;
	}

	public void addPost(Post tmp){
		posts.add(tmp);
	}

	public void removePost(Post tmp){
		posts.remove(tmp);
	}
}