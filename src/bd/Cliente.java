package bd;
import java.io.Serializable;

public class Cliente extends Utilizador implements Serializable{
	long creditCardNumber;
	
	public Cliente(String username, String password, String genero, String email, int idade, int creditCardNumber){
		super(username,password,genero,email,idade);
		this.creditCardNumber = creditCardNumber;
	}
}