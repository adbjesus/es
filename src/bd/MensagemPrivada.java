package bd;
import java.util.*;
import java.io.Serializable;

public class MensagemPrivada extends Conteudo implements Serializable{
	/*Atributos da Classe*/
	Utilizador destinatario;
	String assunto;
	
	/*Construtor da Classe*/
	public MensagemPrivada(Utilizador remetente,String corpo,Date dataDePublicacao, Utilizador destinatario, String assunto){
		super(remetente,corpo,dataDePublicacao);
		this.destinatario = destinatario;
		this.assunto = assunto;
	}
	
	/*Metodos da Classe*/
	
	public Utilizador getDestinatario(){
		return this.destinatario;
	}
	public String getAssunto(){
		return this.assunto;
	}
}
