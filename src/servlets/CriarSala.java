package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;


public class CriarSala extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			Utilizador temp = null;
			String param = "theUser";
			HttpSession session = request.getSession(true);
			String user = (String)session.getAttribute(param);
			String sala = (String)request.getParameter("sala_field");


			System.out.println("searching for room: " + sala);
			Boolean exists = ServletInitializer.salaExists(sala);
			Message msg = new Message();

			if(exists){
				msg.setMessage("Chatroom already exists");
				session.setAttribute("msg", msg);
				response.sendRedirect("/projecto-es/pages/notification_salas.jsp");
				System.out.println(sala + "already exists. ");		
			}else{
				msg.setMessage("Chatroom criada com sucesso");
				session.setAttribute("msg", msg);
				response.sendRedirect("/projecto-es/pages/notification_salas.jsp");
				System.out.println("Adding sala " + sala);
				Sala tmpSala = new Sala(sala,-1,new Date());
				ServletInitializer.adicionarSala(tmpSala);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}