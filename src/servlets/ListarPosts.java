package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;


public class ListarPosts extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			Utilizador temp = null;
			String param = "theUser";
			HttpSession session = request.getSession(true);
			String room = (String)request.getParameter("room_id");
			int roomID = Integer.parseInt(room);
			session.setAttribute("roomID",roomID);
			
			//procurar room
			Sala tmp = ServletInitializer.getRoom(roomID);

			if(tmp != null){
				System.out.println("Listing posts of room " + tmp.getNome());
				//enviar ao utilizador os posts
				session.setAttribute("posts",tmp.getPosts());
				session.setAttribute("current_room",tmp.getNome());
				response.sendRedirect("/projecto-es/pages/sala.jsp");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}