package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;

public class EnviaMensagem extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			Message msg = new Message();
			MensagemPrivada mp;
			HttpSession session = request.getSession(true);
			String destinatario = request.getParameter("dest_field");
			String assunto= request.getParameter("title_field");
			String corpo = request.getParameter("body_field");
			String param="theUser";
			String remetente = (String)session.getAttribute(param);
			Boolean dest=false,rem=false;
			Utilizador temp,envia=null,recebe=null;
			
			System.out.println("Preparando para enviar mensagem...");
			/*Procurar destinatario e remetente*/
			for(int i=0;i<ServletInitializer.u.size();i++){
				temp = ServletInitializer.u.get(i);
				if(dest && rem){
					break;
				}else{
					if(!dest){
						if((temp.getUsername()).compareTo(destinatario)==0){
							System.out.println("Destinatario encontrado..." );
							dest=true;
							recebe=temp;
						}
					}
					if(!rem){
						if((temp.getUsername()).compareTo(remetente)==0){
							System.out.println("Remetente encontrado...");
							rem=true;
							envia=temp;
						}
					}
				}
			}
			
			/*Enviar mensagem e erro caso o destinatario nao exista*/
			if(!dest){
				msg.setMessage("O destinatario nao existe!");
				System.out.println("Envio de mensagem privada falhada de " + remetente + " para " + destinatario + ".");
			}else{
				msg.setMessage("Mensagem enviada com sucesso");
				
				/*Criar Mensagem*/
				mp = new MensagemPrivada(envia,corpo,new Date(),recebe,assunto);
				
				/*Adiconar as Mensagems Enviadas e Recebidas dos respectivos utilizadores*/
				envia.setNewSendMessage(mp);
				recebe.setNewReceivedMessage(mp);
				System.out.println("Mensagem Privada enviada de " + remetente + " para " + destinatario + ".");
			}
			
			session.setAttribute("msg", msg);
			response.sendRedirect("/projecto-es/pages/notification_mensagem.jsp");
			/*System Log*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}