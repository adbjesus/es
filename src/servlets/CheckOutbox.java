package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;

public class CheckOutbox extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Utilizador temp=null;
			String param="theUser";
			String user = (String)session.getAttribute(param);
			
			
			
			/*Procurar utilizador*/
			for(int i=0;i<ServletInitializer.u.size();i++){
				temp=ServletInitializer.u.get(i);
				if(user.compareTo(temp.getUsername())==0){
					System.out.println("Encontrei utilizador...");
					break;
				}
			}
			
			if(temp != null){
				session.setAttribute("out",temp.getOutbox());
			}
			
			response.sendRedirect("/projecto-es/pages/outbox.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}