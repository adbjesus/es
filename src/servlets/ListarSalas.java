package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;


public class ListarSalas extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			Utilizador temp = null;
			String param = "theUser";
			HttpSession session = request.getSession(true);
			
			System.out.println("sending room list..");

			session.setAttribute("rooms",ServletInitializer.getRooms());
			response.sendRedirect("/projecto-es/pages/chatrooms.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}