package servlets;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;

public class Register extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			String username = request.getParameter("username_field");
			String password = request.getParameter("pass_field");
			String genero = request.getParameter("gender_field");
			String email = request.getParameter("mail_field");
			String idade = request.getParameter("age_field");
			boolean exists=false, admin=false;
			HttpSession session = request.getSession(true);
			String administrator = "Admin";
			Message msg = new Message();
			
			int idd = -1;

			boolean validInput = ServletInitializer.validateInput(username,password,genero,email,idade);

			if(!validInput){
				msg.setMessage("Invalid Input Data");
				session.setAttribute("msg", msg);	
				response.sendRedirect("/projecto-es/pages/notification.jsp");
				return;
			}
			
			try{
				idd = Integer.parseInt(idade);
			}catch (Exception e){
				msg.setMessage("Age must be numbers only");
				session.setAttribute("msg", msg);	
				response.sendRedirect("/projecto-es/pages/notification.jsp");
				return;
			}

			//First verify if user exists
			exists = ServletInitializer.userExists(username);

			if(exists){
				msg.setMessage("User already exists! You will now be redirected to the main page...");
			}else{
				boolean chkemail = false;
				boolean chkage = false;
				//verificar se o email e valido
				chkemail = ServletInitializer.checkEmail(email);
				//verifficar se a idade e valida
				chkage = ServletInitializer.checkAge(idd);

				if(chkemail && chkage){
					ServletInitializer.addUser(new Utilizador(username,password,genero,email,idd));
					msg.setMessage("User registered sucessfully! You will now be redirected to the main page...");
				}else{
					if(!chkemail & !chkage ){
						msg.setMessage("Email & Age is not valid! Please try again");	
					}else if(!chkage){
						msg.setMessage("Age is not valid! Enter a number between 10 and 90");
					}else{
						msg.setMessage("Email is not valid! Please try again");
					}
				}
			}
			
			session.setAttribute("msg", msg);	
			response.sendRedirect("/projecto-es/pages/notification.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}