package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;

public class ShowInfo extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			/*Ler utilizador from sessions*/
			Utilizador temp;
			String param="theUser";
			HttpSession session = request.getSession(true);
			String user = (String)session.getAttribute(param);
			
			/*Search user object and send it*/
			for(int i=0;i<ServletInitializer.u.size();i++){
					temp = (Utilizador)ServletInitializer.u.get(i);
					if((temp.getUsername()).compareTo(user) == 0){
						System.out.println("Sending info from user " + user);
						session.setAttribute("user",temp);	
						response.sendRedirect("/projecto-es/pages/info.jsp");
						break;
					}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}