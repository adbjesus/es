package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;

public class ViewMessage extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			String param="theUser";
			HttpSession session = request.getSession(true);
			String user = (String)session.getAttribute(param);
			int id = Integer.parseInt(request.getParameter("id"));
			String box = request.getParameter("var");
			Utilizador temp;
			
			/*Search user object*/
			for(int i=0;i<ServletInitializer.u.size();i++){
					temp = (Utilizador)ServletInitializer.u.get(i);
					if((temp.getUsername()).compareTo(user) == 0){
						if(box.equals("inbox")){
							session.setAttribute("in",temp.getInbox().get(id));
							System.out.println("Sending inbox from" + user);
						}else if(box.equals("outbox")){
							session.setAttribute("in",temp.getOutbox().get(id));
							System.out.println("Sending outbox from" + user);
						}
						session.setAttribute("var",box);
						break;
					}
			}
			
			
			
			response.sendRedirect("/projecto-es/pages/view_messages.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}