package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;

public class Login extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			String passwd = request.getParameter("pass_field");
			String username = request.getParameter("username_field");
			boolean login = false;
			HttpSession session = request.getSession(true);
			
			/*Confirm login*/
			login = ServletInitializer.userLogin(username,passwd);

			if(login){

				if(username.equals("admin")){
					session.setAttribute("type","admin");
				}else{
					session.setAttribute("type","user");
				}

				/*Set Session and redirect do main page*/
				session.setAttribute("theUser", username);
				response.sendRedirect("/projecto-es/pages/main.jsp");
			}else{
				/*Send message bean to user's browser*/
				Message msg = new Message();
				msg.setMessage("Wrong username/password! Please try again...");
				session.setAttribute("msg", msg);
				response.sendRedirect("/projecto-es/pages/notification.jsp");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}