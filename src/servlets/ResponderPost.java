package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;


public class ResponderPost extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);

			String reply_text = request.getParameter("reply_text");
			String post_id = request.getParameter("post_id");
			int postID = -1;
			Message msg = new Message();

			try{
				postID = Integer.parseInt(post_id);
			} catch (Exception e){
				msg.setMessage("Add reply failed");
				session.setAttribute("msg",msg);
				response.sendRedirect("/projecto-es/pages/notification_salas.jsp");
				return;
			}
			System.out.println("POST ID: "+postID);
			int roomID = (int)session.getAttribute("roomID");

			Boolean sucess = ServletInitializer.addReply((String)session.getAttribute("theUser"),reply_text,postID,roomID);
			if(sucess){
				response.sendRedirect("/projecto-es/pages/sala.jsp");
			} else{
				msg.setMessage("Add post failed");
				session.setAttribute("msg",msg);
				response.sendRedirect("/projecto-es/pages/notification_salas.jsp");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		doGet(request,response);
	}

}