package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import bd.*;

public class Logout extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			boolean login = false;
			HttpSession session = request.getSession(true);
			/*Clear current session*/
			session.invalidate();
			response.sendRedirect("/projecto-es/pages/index.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}