package servlets;

import java.io.*;
import java.util.*;
import java.lang.*;
import java.text.SimpleDateFormat;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.MultipartConfig;

import bd.*;


public class ApagarPost extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);

			String post_id = request.getParameter("post_id");

			int postID = Integer.parseInt(post_id);
			int roomID = (int)session.getAttribute("roomID");

			
			Message msg = new Message();
			Boolean sucess = ServletInitializer.deletePost((String)session.getAttribute("theUser"),roomID,postID);
			if(sucess){
				response.sendRedirect("/projecto-es/pages/sala.jsp");
			} else{
				msg.setMessage("Delete post failed");
				session.setAttribute("msg",msg);
				response.sendRedirect("/projecto-es/pages/notification_salas.jsp");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		doGet(request,response);
	}

}