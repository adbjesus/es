package servlets;

import java.io.*;
import java.util.*;
import java.lang.*;
import java.text.SimpleDateFormat;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.MultipartConfig;

import upload.*;
import bd.*;

@MultipartConfig(location = "/tmp", maxFileSize = 10485760L) // 10MB.

public class CriarPost extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);

			MultipartMap map = new MultipartMap(request, this);
			String post_text = map.getParameter("post_text");
			String post_date = map.getParameter("post_date");
			String post_hour = map.getParameter("post_hour");
			String post_minute = map.getParameter("post_minute");
			File post_image = map.getFile("post_image");
			
			int roomID = (int)session.getAttribute("roomID");

			Date date;
			
			if(post_date==null || post_date.equals("")){
				date = new Date();
			}
			else if(post_hour==null || post_hour.equals("")){
				try{
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					date = df.parse(post_date);
				} catch(Exception e){
					e.printStackTrace();
					date = new Date();
				}
			}
			else if(post_minute==null || post_minute.equals("")){
				try{
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH");
					date = df.parse(post_date+" "+post_hour);
				} catch(Exception e){
					e.printStackTrace();
					date = new Date();
				}
			}
			else{
				try{
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					date = df.parse(post_date+" "+post_hour+":"+post_minute);
				} catch(Exception e){
					e.printStackTrace();
					date = new Date();
				}
			}
			System.out.println(date);
			Message msg = new Message();
			Boolean sucess = ServletInitializer.addPost((String)session.getAttribute("theUser"),post_text,date,post_image,roomID);
			if(sucess){
				response.sendRedirect("/projecto-es/pages/sala.jsp");
			} else{
				msg.setMessage("Add post failed");
				session.setAttribute("msg",msg);
				response.sendRedirect("/projecto-es/pages/notification_salas.jsp");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		doGet(request,response);
	}

}