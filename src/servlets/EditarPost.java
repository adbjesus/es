package servlets;

import java.io.*;
import java.util.*;
import java.lang.*;
import java.text.SimpleDateFormat;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.MultipartConfig;

import upload.*;
import bd.*;

@MultipartConfig(location = "/tmp", maxFileSize = 10485760L) // 10MB.

public class EditarPost extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);

			MultipartMap map = new MultipartMap(request, this);
			String post_text = map.getParameter("post_text");
			String post_id = map.getParameter("post_id");
			File post_image = map.getFile("post_image");

			int postID = Integer.parseInt(post_id);
			int roomID = (int)session.getAttribute("roomID");
			System.out.println("ROOM ID: "+roomID);

			Date date;
			
			Message msg = new Message();
			Boolean sucess = ServletInitializer.editPost((String)session.getAttribute("theUser"),post_text,post_image,roomID,postID);
			if(sucess){
				response.sendRedirect("/projecto-es/pages/sala.jsp");
			} else{
				msg.setMessage("Edit post failed");
				session.setAttribute("msg",msg);
				response.sendRedirect("/projecto-es/pages/notification_salas.jsp");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		doGet(request,response);
	}

}