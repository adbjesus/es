package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.ArrayList;
import bd.*;

public class ServletInitializer extends HttpServlet{
	/*Atributos da classe*/
	// Path Windows
	// public static String usersPath = "C:\\Users\\Reabreu\\Documents\\apache-tomcat-7.0.32\\webapps\\projecto-es\\data\\users.obj";
	// public static String salasPath = "C:\\Users\\Reabreu\\Documents\\apache-tomcat-7.0.32\\webapps\\projecto-es\\data\\salas.obj";
	// Path Linux
	public static String usersPath = "/home/alex/Dropbox/Alex/LEI/3ano/1semestre/ES/Projecto/git/data/users.obj";
	public static String salasPath = "/home/alex/Dropbox/Alex/LEI/3ano/1semestre/ES/Projecto/git/data/salas.obj";
	public static String imagePath = "/home/alex/Dropbox/Alex/LEI/3ano/1semestre/ES/Projecto/git/data/images/";
	public static ArrayList <Utilizador> u;
	public static ArrayList <Sala> s;
	
    public void init() throws ServletException{
    	readUsers();
    	readSalas();
		u.add(new Admin("admin","admin","nao aplicavel","admin@projecto-es.com",30));
		/// System Log
		System.out.println("*******");
		System.out.println("*** Es.Project Servlet Initialized sucessfully ***");
		System.out.println("*******");
    }
	
	/*Metodos da classe*/
	public static Utilizador getUser(String nome){
		for(int i=0;i<ServletInitializer.u.size();i++){
			if(ServletInitializer.u.get(i).getUsername().equals(nome)){
				return u.get(i);
			}
		}
		return null;
	}

	public static Post getPost(int postID, int roomID){
		Sala room = getRoom(roomID);
		if(room==null) return null;
		if(room.getPosts().size() > postID)
			return room.getPosts().get(postID);
		return null;
	}

	public static boolean userExists(String nome){
		for(int i=0;i<ServletInitializer.u.size();i++){
			if(ServletInitializer.u.get(i).getUsername().equals(nome)){
				return true;
			}
		}
		return false;
	}
	
	public static boolean userLogin(String nome, String password){
	
		for(int i=0;i<ServletInitializer.u.size();i++){
			if(ServletInitializer.u.get(i).getUsername().equals(nome) && ServletInitializer.u.get(i).getPass().equals(password) ){
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean addUser(Utilizador temp){
		try{
			ServletInitializer.u.add(temp);
			saveData();
			return true;
		}catch(Exception e){
			return false;
		}
	}

	public static boolean checkAge(int age){
		if(age>90 || age<10){
			return false;
		}else{
			return true;
		}
	}

	public static boolean checkEmail(String mail){
		String charset = "[a-zA-Z0-9_]"; // separate this out for future fixes
		String regex = charset + "+@" + charset + "+\\." + charset + "+";

		if(mail.matches(regex)){
			return true;
		}else{
			return false;
		}
	}

	static void saveData(){
		try{
			FileOutputStream fs = new FileOutputStream(usersPath);
			ObjectOutputStream oS = new ObjectOutputStream(fs);
			oS.writeObject(u);
			fs.close();
			oS.close();

			fs = new FileOutputStream(salasPath);
			oS = new ObjectOutputStream(fs);
			oS.writeObject(s);
			fs.close();
			oS.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	static void readUsers(){
		try{ 
			FileInputStream fs = new FileInputStream(usersPath);
			ObjectInputStream is = new ObjectInputStream(fs);
			u = (ArrayList <Utilizador>)is.readObject();
			is.close();
			fs.close();
		} catch (Exception e){
			e.printStackTrace();
			u = new ArrayList <Utilizador>();
		}
	}

	static void readSalas(){
		try{ 
			FileInputStream fs = new FileInputStream(salasPath);
			ObjectInputStream is = new ObjectInputStream(fs);
			s = (ArrayList <Sala>)is.readObject();
			is.close();
			fs.close();
		} catch (Exception e){
			e.printStackTrace();
			s = new ArrayList <Sala>();
		}
	}

	public static boolean salaExists(String tmp){
		for(int i=0; i<ServletInitializer.s.size();i++){
			if((ServletInitializer.s.get(i)).getNome().equals(tmp)){
				return true;
			}
		}
		return false;
	}

	public static void adicionarSala(Sala tmp){
		ServletInitializer.s.add(tmp);
		saveData();
	}

	public static ArrayList <Sala> getRooms(){
		return ServletInitializer.s;
	}

	public static Sala getRoom(int id){
		int size = ServletInitializer.s.size();

		if (size > id){
			return ServletInitializer.s.get(id);
		}else{
			return null;
		}
	}

	public static boolean validateInput(String username,String password, String genero, String email, String idade){
		if(username==null 	|| username.equals("") 											||
			password==null	|| password.equals("") 											||
			genero== null 	|| (!genero.equals("Feminino") && !genero.equals("Masculino")) 	||
			idade == null 	|| idade.equals("")												||
			email==null 	|| email.equals("")){
				//msg.setMessage("Invalid Input Data");
				//session.setAttribute("msg", msg);	
				//response.sendRedirect("pages/notification.jsp");
				return false;
		}else{
				return true;
		}
	}

	public static Boolean addPost(String user, String post_text,Date date,File post_image, int roomID){
		try{
			Date d = new Date();
			Post tmp = new Post(getUser(user),post_text,date,post_image,roomID+"_"+post_text+"_"+d.getTime());
			s.get(roomID).addPost(tmp);
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		saveData();
		return true;
	}

	public static Boolean deletePost(String user,int roomID, int postID){
		try{
			Post tmp = getPost(postID,roomID);
			s.get(roomID).removePost(tmp);
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		saveData();
		return true;
	}

	public static Boolean addReply(String user, String reply_text,int postID, int roomID){
		try{
			Post original = getPost(postID,roomID);
			Post tmp = new Post(getUser(user),reply_text,new Date(),null,null);
			System.out.println(reply_text);
			original.addReply(tmp);
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		saveData();
		return true;
	}

	public static Boolean editPost(String user, String post_text, File post_image,  int roomID, int postID){
		try{
			Date d = new Date();
			Post original = getPost(postID,roomID);
			original.editPost(post_text,post_image,roomID+"_"+post_text+"_"+d.getTime());
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		saveData();
		return true;
	}
}